const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const { resCustom, customResponse } = require("../utilities/response");
const { postUser } = require("../models/users");
const authModel = require("../models/auth");

const register = async (req, res) => {
  try {
    const { email, password } = req.body;
    const hashPassword = bcrypt.hashSync(password, 10);
    const register = await postUser({ email, password: hashPassword });

    const response = customResponse(200, "Success", register);
    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Failed Register" });
  }
};

const login = async (req, res) => {
  // console.log(req.body);
  try {
    const { email, password } = req.body;
    const getData = await authModel.login(email);

    //check account active or not
    if (getData[0].isActive != 1) {
      return resCustom(res, { status: 500, msg: "Failed Login" });
    }

    //compare password
    const verified = bcrypt.compareSync(password, getData[0].password);
    if (!verified) {
      console.log("oke2");
      return resCustom(res, { status: 500, msg: "Failed Login" });
    }
    //get token
    const token = jwt.sign(
      {
        id: getData[0].id,
        github_id: getData[0].github_id,
        role: getData[0].role,
      },
      process.env.SECRET
    );
    const response = customResponse(200, "success", token);
    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Failed Login" });
  }
};

const oAuthValidate = async (req, res) => {
  try {
    const { github_id } = req.body;
    const data = await authModel.getValidate(github_id);
    console.log();
    if (Object.keys(data).length === 0) {
      const response = customResponse(200, "success", "null");

      resCustom(res, response);
    } else {
      const response = customResponse(200, "success", data);

      resCustom(res, response);
    }
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

const oAuthRegister = async (req, res) => {
  try {
    await postUser(req.body);
    const response = customResponse(200, "success");

    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

const RegisterOAuth = async (req, res) => {
  try {
    const { email, password, github_id } = req.body;
    const hashPassword = bcrypt.hashSync(password, 10);
    await authModel.patchAuth(github_id, { email, password: hashPassword });
    const response = customResponse(200, "success");

    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

const loginOAuth = async (req, res) => {
  try {
    const token = jwt.sign(
      {
        id: req.body.id,
        github_id: req.body.github_id,
        role: req.body.role,
      },
      process.env.SECRET
    );
    console.log(token);
    const response = customResponse(200, "success", token);
    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

module.exports = {
  register,
  login,
  loginOAuth,
  oAuthRegister,
  oAuthValidate,
  RegisterOAuth,
};
