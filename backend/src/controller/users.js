const userModel = require("../models/users");
const { customResponse, resCustom } = require("../utilities/response");

const getUsers = async (req, res) => {
  try {
    const id = req.token.id || 0;
    const users = await userModel.getUsers(id);
    const response = customResponse(200, "Success", users);

    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};
const getUser = async (req, res) => {
  try {
    const id = req.token.id || 0;
    const github_id = req.body.github_id || 1;
    const user = await userModel.getUser(id, github_id);
    const response = customResponse(200, "success", user);

    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

const patchUser = async (req, res) => {
  try {
    const { id } = req.token;
    await userModel.patchUser(id, req.body);
    const response = customResponse(200, "success");

    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

const postUser = async (req, res) => {
  try {
    await userModel.postUser(req.body);
    const response = customResponse(200, "success");

    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

const deleteUser = async (req, res) => {
  try {
    await userModel.deleteUser(req.body.id);
    const response = customResponse(200, "success");

    resCustom(res, response);
  } catch {
    resCustom(res, { status: 500, msg: "Bad Request" });
  }
};

module.exports = { getUsers, getUser, patchUser, postUser, deleteUser };
