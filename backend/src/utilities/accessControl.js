const { resCustom } = require("../utilities/response");

const accessControl = async (req, res, next) => {
  const { role } = req.token;
  console.log(req.token);
  if (role === 13) {
    next();
  } else {
    resCustom(res, {
      status: 500,
      msg: "Bad Request",
      data: "Your Role must be Super Admin to Access this Feature.",
    });
  }
};

module.exports = { accessControl };
