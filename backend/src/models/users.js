const query = require("../utilities/query");

module.exports = {
  getUsers: (id) =>
    query(
      "SELECT id, email, name, role, github_id, username, url, photo FROM users WHERE isActive=1 AND id <> ? ORDER BY email ASC",
      id
    ),
  getUser: (id, github_id) =>
    query(
      "SELECT email, name, role, github_id, username, url, photo FROM users WHERE id = ? OR github_id = ?",
      [id, github_id]
    ),
  postUser: (data) => query("INSERT INTO users SET ?", data),
  patchUser: (id, data) => query("UPDATE users SET ? WHERE id=?", [data, id]),
  deleteUser: (id) => query("UPDATE users SET isActive=0 WHERE id=?", id),
};
