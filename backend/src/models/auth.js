const query = require("../utilities/query");

module.exports = {
  login: (email) =>
    query(
      "SELECT id, github_id, password, isActive, role FROM users WHERE email= ?",
      email
    ),
  getValidate: (github_id) =>
    query(
      "SELECT id, github_id, email, role FROM users WHERE github_id = ? ",
      github_id
    ),
  patchAuth: (github_id, data) =>
    query("UPDATE users SET ? WHERE github_id=?", [data, github_id]),
};
