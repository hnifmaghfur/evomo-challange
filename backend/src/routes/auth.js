var express = require("express");
var router = express.Router();

const authRouter = require("../controller/auth");

router.post("/register", authRouter.register);
router.post("/login", authRouter.login);
router.post("/oauth_register", authRouter.oAuthRegister);
router.patch("/oauth_register", authRouter.RegisterOAuth);
router.post("/oauth_login", authRouter.loginOAuth);
router.get("/oauth_validate", authRouter.oAuthValidate);

module.exports = router;
