var express = require("express");
var router = express.Router();

const userController = require("../controller/users");
const { accessControl } = require("../utilities/accessControl");
const { validate } = require("../utilities/validate");

/* GET users listing. */
router.get("/", validate, userController.getUser);
router.get("/all", validate, userController.getUsers);
router.patch("/", validate, accessControl, userController.patchUser);
router.post("/", validate, accessControl, userController.postUser);
router.patch("/delete", validate, accessControl, userController.deleteUser);

module.exports = router;
