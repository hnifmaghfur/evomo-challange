var express = require("express");
var router = express.Router();
const service = require("../services");
const passport = require("passport");

//Route Pages
router.get("/login", function (req, res, next) {
  const data = {
    title: "express",
  };

  res.render("login", data);
});
router.get("/logout", function (req, res, next) {
  req.logOut();
  res.redirect("/login");
});
router.get("/register", function (req, res, next) {
  res.render("register");
});

router.get("/dashboard", function (req, res, next) {
  res.render("dashboard");
});

//Route services
router.post("/services/login", service.login);
router.get("/services/delete", service.deleteUsers);
router.post("/services/register", service.register);
router.post("/services/register/oauth", service.registerOAuth);
router.get("/services/dashboard", service.dashboard);
router.get(
  "/services/auth/github",
  passport.authenticate("github", { failureRedirect: "/login" })
);
router.get(
  "/services/auth/github/callback",
  passport.authenticate("github", { failureRedirect: "/login" }),
  service.oAuthLogin,
  service.oAuth
);

module.exports = router;
