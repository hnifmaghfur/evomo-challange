const GitHubStrategy = require("passport-github").Strategy;
const localStorage = require("local-storage");
const axios = require("axios").default;

const login = async (req, res) => {
  try {
    let data = req.body;
    const dataToken = await axios({
      method: "post",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/auth/login`,
      data: {
        email: data.email,
        password: data.password,
      },
    });
    localStorage.set("token", dataToken.data.data);
    res.redirect("/services/dashboard");
  } catch {
    console.log(new Error("error"));
  }
};

const register = async (req, res) => {
  try {
    let { email, password } = req.body;
    const response = await axios({
      method: "post",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/auth/register`,
      data: {
        email: email,
        password: password,
      },
    });

    if (!response) {
      return res.render("register", { message: "Failed Register" });
    }

    res.render("login", { message: "Success Register" });
  } catch {
    res.render("register", { message: "Failed Register" });
  }
};

const registerOAuth = async (req, res) => {
  console.log(req.body);
  try {
    let { email, password, github_id } = req.body;
    await axios({
      method: "patch",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/auth/oauth_register`,
      data: {
        email: email,
        password: password,
        github_id: github_id,
      },
    });

    res.redirect("/login");
  } catch {
    res.render("register", { message: "Failed Register" });
  }
};

const oGithubSession = {
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: true,
    httpOnly: true,
    maxAge: 5 * 60 * 1000,
  },
};

const oGithubSettings = new GitHubStrategy(
  {
    clientID: process.env.GITHUB_CLIENT_ID,
    clientSecret: process.env.GITHUB_CLIENT_SECRET,
    callbackURL: `${process.env.URL}/services/auth/github/callback`,
  },
  function (accessToken, refreshToken, profile, cb) {
    let {
      id,
      displayName: name,
      username,
      profileUrl: url,
      photos: dataPhoto,
    } = profile;
    let photo = dataPhoto[0].value;
    // console.log(id);
    // console.log(name);
    // console.log(username);
    // console.log(url);
    // console.log(photo);
    cb(null, profile);
  }
);

const dashboard = async (req, res) => {
  // console.log(localStorage.get("token"));
  try {
    const token = localStorage.get("token");
    const getData = await axios({
      method: "get",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/users`,
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    const getDataArray = await axios({
      method: "get",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/users/all`,
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    const dataArray = getDataArray.data.data;
    const data = getData.data.data;
    // console.log(dataArray[0]);

    const roleName =
      data[0].role == process.env.ROLE_SUPERADM
        ? "Super Admin"
        : data[0].role == process.env.ROLE_ADM
        ? "Admin"
        : data[0].role == process.env.ROLE_DIR
        ? "Director"
        : data[0].role == process.env.ROLE_HOE
        ? "Head Of Engineer"
        : "Operator";

    res.render("dashboard", {
      users: dataArray,
      email: data[0].email,
      name: data[0].name,
      username: data[0].username,
      roleName: roleName,
      github_id: data[0].github_id,
      url: data[0].url,
      photo: data[0].photo,
    });
  } catch {
    res.redirect("/login");
  }
};

const oAuth = async (req, res) => {
  try {
    // console.log(req.user._json);
    const {
      id,
      login: username,
      name,
      html_url: url,
      avatar_url: photo,
    } = req.user._json;
    const data = {
      github_id: id,
      username: username,
      name: name,
      url: url,
      photo: photo,
    };
    const token = localStorage.get("token");
    await axios({
      method: "patch",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/users/`,
      headers: {
        authorization: `Bearer ${token}`,
      },
      data: data,
    });
    res.redirect("/services/dashboard");
  } catch {
    res.redirect("/services/dashboard");
  }
};

const oAuthLogin = async (req, res, next) => {
  try {
    const { id: github_id } = req.user._json;
    const getData = await axios({
      method: "get",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/auth/oauth_validate`,
      data: {
        github_id: github_id,
      },
    });

    const data = getData.data.data;
    if (data === "null") {
      const {
        id,
        login: username,
        name,
        html_url: url,
        avatar_url: photo,
      } = req.user._json;
      const dataRegis = {
        github_id: id,
        username: username,
        name: name,
        url: url,
        photo: photo,
      };
      const response = await axios({
        method: "post",
        url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/auth/oauth_register`,
        data: dataRegis,
      });
      console.log(response);
      res.render("oauth", { username: username, github_id: github_id });
    }
    console.log(Object.keys(data).length > 0);
    if (Object.keys(data).length > 0) {
      const dataToken = await axios({
        method: "post",
        url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/auth/oauth_login`,
        data: {
          id: data[0].id,
          github_id: data[0].github_id,
          role: data[0].role,
        },
      });
      localStorage.set("token", dataToken.data.data);
      res.redirect("/services/dashboard");
    }

    next();
  } catch {
    res.redirect("/login");
  }
};

const deleteUsers = async (req, res) => {
  console.log(parseInt(req.query.id));
  console.log(typeof parseInt(req.query.id));
  try {
    const token = localStorage.get("token");
    const data = parseInt(req.query.id);
    await axios({
      method: "patch",
      url: `${process.env.URL_BACKEND}${process.env.PREFIX_URL}/users/delete`,
      headers: {
        authorization: `Bearer ${token}`,
      },
      data: {
        id: data,
      },
    });
    res.redirect("/services/dashboard");
  } catch {
    res.redirect("/services/dashboard");
  }
};

module.exports = {
  login,
  register,
  deleteUsers,
  registerOAuth,
  dashboard,
  oAuth,
  oAuthLogin,
  oGithubSession,
  oGithubSettings,
};
