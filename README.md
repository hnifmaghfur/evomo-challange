# EVOMO Task

## Feature

- Login with OAuth Github and Access Control
- Email Notification Service

## How to use Login with OAuth Github

1. Clone this repository.
2. Place database.sql to mysql database.
3. Give table name `evomo`.
4. Open `backend` folder and running commend `yarn` to install package.
5. Open `frontend` folder and do like `backend`
6. Run `yarn start` to running program.
7. You can see login page in `http://localhost:5000/login`
8. You can see backend page in `http://localhost:5050`

### Access Account

- all email with password `123123123`.

## How to use Email Notification Services

1. Clone this repository.
2. Open `notifications` folder and running
   commend `yarn` to install package.
3. make `.env` file on `notifications` folder to place your email and password as sender. (you can see `.env.example` for examples)
4. Run `yarn start` to running program.
5. You can see email page in `http://localhost:3000/`
