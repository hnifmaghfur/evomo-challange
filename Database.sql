-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2021 at 02:37 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evomo`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `github_id` int(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(100) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `role` int(11) NOT NULL DEFAULT 70,
  `isActive` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `github_id`, `email`, `password`, `username`, `name`, `url`, `photo`, `role`, `isActive`) VALUES
(1, 0, 'akuhanif@gmail.com', '$2b$10$H6/lFpgEC2lYmedA8f0OXuYB8eIvyUs9Y3UAj4b58hCFg.Bndh0RO', '', '', '', '', 70, 1),
(2, 0, 'aku@hanif.com', '$2b$10$c33Ron9KAJQ76okQbw2M1eon9phl6mBEl0F4K91lz23p1CbzsqVam', 'hnifmaghfur', 'Hanif Maghfur', 'https://github.com/hnifmaghfur', 'https://avatars.githubusercontent.com/u/46650073?v=4', 70, 1),
(6, 0, 'trial@gmail.com', '$2b$10$ZFHx.e/NOjP/GcwtmCz97.DFEXlz1KY9p.xp6i0iytvCbRwZg2Lx6', '', '', '', '', 70, 1),
(7, 0, 'aevraury10@gmail.com', '$2b$10$ORrIOlUy9l8fJaaz.Eqo5OkKp9Pkuxe.vyMWGnnjOviTyx5tlWPW6', '', '', '', '', 70, 0),
(46650081, 46650073, 'hnifmaghfur@gmail.com', '$2b$10$8Rgvr8w9QKVHBBbNcEEO4eEgPx6fa3zULkpb0FqQGbUWkp.tLBRuS', 'hnifmaghfur', 'Hanif Maghfur', 'https://github.com/hnifmaghfur', 'https://avatars.githubusercontent.com/u/46650073?v=4', 13, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46650082;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
