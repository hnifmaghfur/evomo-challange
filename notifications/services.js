const nodemailer = require("nodemailer");

const smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: `${process.env.EMAIL}`,
    pass: `${process.env.PASS}`,
  },
});

const mailer = async (req, res) => {
  try {
    smtpTransport.sendMail(
      {
        from: process.env.EMAIL,
        to: `${req.body.email}`,
        subject: `${req.body.subject}`,
        html: `<p>message: ${req.body.message}</p>`,
      },
      (err, res) => {
        if (err) {
          console.log(err);
        } else {
          console.log(`Message sent.`);
        }
      }
    );
    smtpTransport.close();
    res.render("success", {
      email: process.env.EMAIL,
      receiver: req.body.email,
    });
  } catch {
    res.send("Failed");
  }
};

module.exports = { smtpTransport, mailer };
