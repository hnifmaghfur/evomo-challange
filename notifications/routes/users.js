var express = require("express");
const { mailer } = require("../services");
var router = express.Router();

/* GET users listing. */
router.post("/mail", mailer);

module.exports = router;
